# Copyright 2009 Elias Pipping <elias@pipping.org>
# Copyright 2013 Thomas Witt
# Distributed under the terms of the GNU General Public License v2

MY_PNV="${PN}_${PV}_all_"

require cmake
require vala [ vala_dep=true ]
require gtk-icon-cache

SUMMARY="A lightweight, webkit-based web browser"
DESCRIPTION="
An upcoming web browser, not yet ready for productive use.
Based on WebKit and GTK+
"

REMOTE_IDS="launchpad:midori"

HOMEPAGE="http://midori-browser.org"

if ever is_scm;then
    SCM_REPOSITORY="http://bazaar.launchpad.net/~midori/midori/"
    require scm-bzr
else
    DOWNLOADS="${HOMEPAGE}/downloads/${MY_PNV}.tar.bz2"
fi

SLOT="0"
LICENCES="LGPL-2.1"
MYOPTIONS="
    doc
    webkit2 [[ description = [ Enable webkit2 support (EXPERIMENTAL) ] ]]
"
# webkit2: HALF BROken and INCOMplete, considered "developer only" by upstream

DEPENDENCIES="
    build:
        dev-util/intltool
        sys-devel/gettext
        virtual/pkg-config
        doc? ( dev-doc/gtk-doc )
    build+run:
        dev-db/sqlite:3[>=3.6.19]
        dev-libs/glib:2[>=2.23.3]
        dev-libs/libxml2:2.0[>=2.6]
        gnome-desktop/gcr[>=2.32]
        gnome-desktop/librsvg:2
        gnome-desktop/libsoup:2.4[>=2.48.0]
        x11-libs/gtk+:3[>=3.10.0]
        x11-libs/libnotify
        webkit2? ( net-libs/webkit:4.0[>=2.3.91] )
        !webkit2? ( net-libs/webkit:3.0[>=2.0.3] )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DUSE_GTK3:BOOL=TRUE
    -DUSE_ZEITGEIST:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTIONS=(
    'doc USE_APIDOCS'
    'webkit2 HALF_BRO_INCOM_WEBKIT2'
)

# Tests require access to the running xserver
RESTRICT="test"

