# Copyright 2009 Elias Pipping <elias@pipping.org>
# Copyright 2012 Ali Polatel <alip@exherbo.org>
# Copyright 2015 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gtk-icon-cache
require option-renames [ renames=[ 'xfce-panel xfce4-panel' ] ]
require xfce [ module=xfce ]

SUMMARY="Power manager for the Xfce desktop"
DESCRIPTION="This software is a power manager for the Xfce desktop, Xfce power manager manages the
power sources on the computer and the devices that can be controlled to reduce their power
consumption (such as LCD brightness level, monitor sleep, CPU frequency scaling). In addition,
xfce4-power-manager provides a set of freedesktop-compliant DBus interfaces to inform other
applications about current power level so that they can adjust their power consumption, and it
provides the inhibit interface which allows applications to prevent automatic sleep actions via the
power manager; as an example, the operating system's package manager should make use of this
interface while it is performing update operations."
HOMEPAGE="http://docs.xfce.org/xfce/${PN}/start"

SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    networkmanager [[ description = [ Allow for informing NetworkManager when hibernating/suspending ] ]]
    polkit [[ description = [ Use polkit to hibernate/suspend/etc. ] ]]
    xfce4-panel [[ description = [ Build a panel plugin for xfce4-panel. ] ]]
"

DEPENDENCIES="
    build+run:
        dev-libs/glib:2[>=2.30.0]
        sys-apps/dbus[>=1.1]
        sys-apps/upower[>=0.9.7]
        x11-libs/gtk+:3[>=3.10.0]
        x11-libs/libnotify[>=0.4.1]
        x11-libs/libXrandr[>=1.2.0]
        xfce-base/libxfce4util[>=4.10.0]
        xfce-base/libxfce4ui[>=4.10.0][gtk3]
        xfce-base/xfconf[>=4.10.0]
        networkmanager? ( net-apps/NetworkManager )
        polkit?         ( sys-auth/polkit )
        xfce4-panel?    ( xfce-base/xfce4-panel[>=4.10.0][gtk3] )
    suggestion:
        x11-apps/light-locker [[
            description = [ Light Locker can integrate with ${PN}'s settings ]
        ]]
"

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'networkmanager network-manager'
    polkit
    'xfce4-panel xfce4panel'
)

